# usage


# INPUT:
# generate_code_errors directory  calibrationSpecKey 
# direcotry is the folder that contains the images
# calibrationSpecKey (along with the smartphone model name) is part of the key into the calibration table

# 
# directory that contains the image data should also contain a comma delimited file (markers_metadata.csv) which describes
# the expected location of the markers

# OUTPUT
# 
# a file called "markerLocations.txt" is created in the folder that contains the locations of the markers for ground truthing
# debug images photos are also created that show the quality of the codes


# MISCELLANEOUS
# need to add a new entry to the distotionParams[] table for every additional (camera,calibration) combination
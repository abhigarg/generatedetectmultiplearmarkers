

# usage


# INPUT:
# generate_code_errors directory  calibrationSpecKey 
# direcotry is the folder that contains the images
# calibrationSpecKey (along with the smartphone model name) is part of the key into the calibration table

# 
# directory that contains the image data should also contain a comma delimited file (markers_metadata.csv) which describes
# the expected location of the markers

# OUTPUT
# 
# a file called "markerLocations.txt" is created in the folder that contains the locations of the markers for ground truthing
# debug images photos are also created that show the quality of the codes


# MISCELLANEOUS
# need to add a new entry to the distotionParams[] table for every additional (camera,calibration) combination




from __future__ import print_function

import numpy as np

import cv2

import sys
import os

import scipy.spatial.distance as spsd


import csv
import re
import PIL.ExifTags


 


def Distort(undistX,undistY,fx,fy,cx,cy,k1,k2,k3,p1,p2):
    
    x=(undistX-cx)/fx
    y=(undistY-cy)/fy
    
    r2 = x*x* + y*y
    
    xDistort = x * (1 + k1 * r2 + k2 * r2 * r2 + k3 * r2 * r2 * r2)
    yDistort = y * (1 + k1 * r2 + k2 * r2 * r2 + k3 * r2 * r2 * r2)
    
    # Tangential distorsion
    xDistort = xDistort + (2 * p1 * x * y + p2 * (r2 + 2 * x * x));
    yDistort = yDistort + (p1 * (r2 + 2 * y * y) + 2 * p2 * x * y);
#
    # Back to absolute coordinates.
    xDistort = xDistort * fx + cx;
    yDistort = yDistort * fy + cy;
    
    return(xDistort,yDistort)
    



def persTransformCorners(M, pts):
	

    foo=np.zeros((1,1,2))
    out=np.zeros((1,1,2))
    	
    foo[0,0,0]=pts[0,0]
    foo[0,0,1]=pts[0,1]
    
    outList=[]
         
         
    out=cv2.perspectiveTransform(foo,M)
    outList.append(out)
    print ("\n77777777777777777666666666666666", "\n", foo,"\n", out, "Shape", out.shape, "\n", M)
     
    foo[0,0,0]=pts[1,0]
    foo[0,0,1]=pts[1,1]
    	
     
    out=cv2.perspectiveTransform(foo,M)
    outList.append(out)
    print ("\n77777777777777777666666666666666", "\n", foo,"\n", out, "\n", M)
     
     
    foo[0,0,0]=pts[2,0]
    foo[0,0,1]=pts[2,1]
    	
     
    out=cv2.perspectiveTransform(foo,M)
    outList.append(out)
    print ("\n77777777777777777666666666666666", "\n", foo,"\n", out, "\n", M)
     
     
    foo[0,0,0]=pts[3,0]
    foo[0,0,1]=pts[3,1]
    	
     
    out=cv2.perspectiveTransform(foo,M)
    outList.append(out)
    print ("\n77777777777777777666666666666666", "\n", foo,"\n", out, "\n", M)
     
     
    print ("\n333333333333 Warping Matrix\n", M, "\n")
    print (M)
    print ("\n77773333333333333333333333330000000000", outList)
    
   
   
    
    
    return(outList)










def computeDest(tl, tr, br, bl):
# compute the width of the new image, which will be the
	# maximum distance between bottom-right and bottom-left
	# x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

	# compute the height of the new image, which will be the
	# maximum distance between the top-right and bottom-right
    	# y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
 

	# now that we have the dimensions of the new image, construct
	# the set of destination points to obtain a "birds eye view",
	# (i.e. top-down view) of the image, again specifying points
	# in the top-left, top-right, bottom-right, and bottom-left
	# order
    dst = np.array([
    		[0, 0],
    		[maxWidth - 1, 0],
    		[maxWidth - 1, maxHeight - 1],
    		[0, maxHeight - 1]], dtype = "float32")

    return((dst,maxHeight, maxWidth))
 


def rectify(h):
#        print "\n$$$$$$$$$$ rectify \n", h
        h = h.reshape((4,2))
        hnew = np.zeros((4,2),dtype = np.float32)
 
        add = h.sum(1)
        hnew[0] = h[np.argmin(add)]
        hnew[2] = h[np.argmax(add)]
#        print "\nAdd hnew\n",add, "\n",hnew[0],hnew[2]
         
        diff = np.diff(h,axis = 1)
        hnew[1] = h[np.argmin(diff)]
        hnew[3] = h[np.argmax(diff)]
        #print "\n$$$$$$$$$$ rectify \n", h
#        print "\nDiff hnew\n",diff, "\n",hnew[1],hnew[3]
#        print "\nResult \n",hnew
  
        return hnew

def perp( a ) :
    b = np.empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b

# line segment a given by endpoints a1, a2
# line segment b given by endpoints b1, b2
# return 
def seg_intersect(a1,a2, b1,b2) :
    print ("\n a1,a2, b1,b2", a1,a2, b1,b2, "\n")
    da = a2-a1
    db = b2-b1
    dp = a1-b1
    dap = perp(da)
    denom = np.dot( dap, db)
    num = np.dot( dap, dp )
    return (num / denom.astype(float))*db + b1


def findSide(pt0,pt1,cntitem):
    #print "\n findside", pt0,pt1,"\n ", cntitem,"\n"
    for i in range (cntitem.shape[0]):

        xval=cntitem[i][0]
        yval=cntitem[i][1]
        #print "\n xval yval ", xval," ",yval, " ", pt0[0], " ", pt0[1],"\n"
        if ( (xval==pt0[0] )  and (yval==pt0[1]) ):
            index0=i
            break
    
    for i in range (cntitem.shape[0]):
        xval=cntitem[i][0]
        yval=cntitem[i][1]
        if ( (xval==pt1[0] )  and (yval==pt1[1]) ):
            index1=i
            break
    
#    print "findeside end",index0," ",index1
    return(index0,index1)


def getBestFitLineForRectSide(ptsonlrs):
    _
#
#    extremex=ptsonline[200,0,0]
#
#    [vx,vy,x,y] = cv2.fitLine(ptsonlrs,cv2.DIST_FAIR,0,0.01,0.01)
#    # Now find two extreme points on the line to draw line
#    lefty = int((-x*vy/vx) + y)
#    righty = int(((    extremex        -x)*vy/vx)+y)
#    
#    
#    #Finally draw the line
#    #cv2.line(undistimg,(   extremex  -1,righty),(0,lefty),255,2)
#    cv2.line(undistimg,( x+100*vx, y+400*vy),(x,y),255,2)
#    #cv2.line(undistimg,(0,0),(5000,5000),255,5)
#    cv2.imwrite("c:\\temp\\lineq.png", undistimg)
#    return( (vx,vy,x,y) )


def getIntersection(line1, line2):
    s1 = np.array(line1[0])
    e1 = np.array(line1[1])

    s2 = np.array(line2[0])
    e2 = np.array(line2[1])

    a1 = (s1[1] - e1[1]) / (s1[0] - e1[0])
    b1 = s1[1] - (a1 * s1[0])

    a2 = (s2[1] - e2[1]) / (s2[0] - e2[0])
    b2 = s2[1] - (a2 * s2[0])

    if abs(a1 - a2) < sys.float_info.epsilon:
        return False

    x = (b2 - b1) / (a1 - a2)
    y = a1 * x + b1
    return (x, y)
 
 
def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1]) #Typo was here

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
       raise Exception('lines do not intersect')

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y


def boxFormContourBox1(cnt,boxID, codeBoxArray1,maxNumberOfPtsPerSide):
   
    corners = cv2.approxPolyDP(cnt,0.01*cv2.arcLength(cnt,True),True)
 
    
    corners=rectify(corners)
#    print "\n555555555 Corners 555555555\n", corners,corners[1,1]
    
    
    
    cntFlatten= cnt.flatten()
    shp = cntFlatten.shape
    cntFlatten=cntFlatten.reshape( (  (shp[0]/2),2  ) )
#    print "\n cnt flatten shape", shp, cntFlatten.shape


    for i in range(4):
        index0,index1=findSide(corners[i], corners[(i+1)%4],cntFlatten) 
#        print "\n index 0 indexc 1", index0 , index1
        #print "\n444 corners", corners[i][0], corners[(i+1)%4][0],"\n"
        #print "\n777777777777777 i = 777777777", i, index0,index1
   
        if (index0< index1):
            ptsonline=cnt[index0:index1]
            #print "\n @@@@@@@@@@@@@@@@@@@@@@@ pts on line ", ptsonline
        else:
            iseq1=np.arange(index0,cnt.shape[0]-1,1)
            iseq2=np.arange(0,index1,1)
            iseq3=np.concatenate((iseq1,iseq2))
            ptsonline=cnt[iseq3]
           
        
        ptsonlff=ptsonline.flatten()
        ptsonlrs=ptsonlff.reshape(    len(ptsonlff)/2  ,2)
#        print "\n ########## pts on line shape ", ptsonlrs.shape
        
        ptsInInterval=np.zeros( (maxNumberOfPtsPerSide,2))
        ptsInInterval[0:len(ptsonlff)/2]=ptsonlrs
        

             
        codeBoxArray1[boxID,i]=ptsInInterval
        

    
    return



def graphLine1(img,vx,vy,x,y,clr,thckness,pointOnLine):

        
    
    xMax=img.shape[1]
    yMax=img.shape[0]
    
    result=[]
    
    nY0= -y/vy
    xY0 = x + nY0*vx
    result.append( (xY0,0) )
    
    nyMax= (yMax-y)/vy
    xyMax = x + nyMax*vx
    result.append((xyMax,yMax))
    
    nX0= -x/vx
    yX0 = y + nX0*vy
    result.append((0,yX0))
    
    nxMax= (xMax-x)/vx
    yxMax = y + nxMax*vy
    result.append((xMax, yxMax))
    
    resultList = [item for item in result if (item[0]>=0) & (item[1]>=0) & (item[0]<=xMax) & (item[1]<=yMax)    ]
    
    resultP1=( (int(resultList[0][0]), int(resultList[0][1]) ) )
    resultP2=( (int(resultList[1][0]), int(resultList[1][1]) ) )
    resultP1f=( ((resultList[0][0]), (resultList[0][1]) ) )
    resultP2f=( ((resultList[1][0]), (resultList[1][1]) ) )

    
    
      
    cv2.line(img,resultP1,resultP2 ,clr,2)
    
    return(  resultP1f, resultP2f)
   



def graphLine1Old(img,vx,vy,x,y,clr,thckness,pointOnLine):

    
    maxX=img.shape[1]
    minX=0
    maxY=img.shape[0]
    minY=0
    
#    print "\n#################################  graphline 1"
#    print "\nvx,vy,x,y,clr,thckness,pointOnLine", vx,vy,x,y,clr,thckness
#    print "\nminX,maxX,minY,maxY", minX,maxX,minY,maxY
    my = vy/vx
    
#    print "\n my is ", my
    
    n=(maxY-y)/my
    xMaxY = x+n
    #print "\n n xMaxY", n,xMaxY
    n=(minY-y)/my
    xMinY = x+n
    #print "\n n xMinY", n,xMinY
    
    mx = vx/vy
    
    #print "\n mx is ", mx
    
    n=(maxX-x)/mx
    yMaxX = y+n
    #print "\n n yMaxX", n,yMaxX
    n=(minX-x)/mx
    yMinX = y+n
    #print "\n n yMinX", n,yMinX
    
     
    
    if ( (xMinY*np.sign(vx)>minX) and (xMaxY*np.sign(vy)<maxX)):
        resultP1=( (int(xMinY), int(minY) ) )
        resultP2=( (int(xMaxY), int(maxY))  )
        resultP1f=( ((xMinY), (minY) ) )
        resultP2f=( ((xMaxY), (maxY))  )
        
        
    else:
        resultP1=( (int(minX), int(yMinX) ) )
        resultP2=( (int(maxX), int(yMaxX))  )
        resultP1f=( ((minX), (yMinX) ) )
        resultP2f=( ((maxX), (yMaxX))  )
        

        
    #print "\n Result is ", resultP1, resultP2
   
    cv2.line(img,resultP1,resultP2 ,clr,2)
    
    return(  resultP1f, resultP2f)


def AssociateBoxesWithRowsColumns(img,cbal):
#    
#    leftMostPointsFromBox=codeBoxArray[rowNumber,columnNumber,3].flatten() [np.flatnonzero(codeBoxArray[rowNumber,columnNumber,3])].reshape(-1,2)
#
    # find y interecepts
    maxYInterceptGapForRow = 20

    cntsYIntercepts=[]
    for cntItem in cbal:
        #topMostPoints=cntItem[0]   
        topMostPoints=cntItem[0].flatten() [np.flatnonzero(cntItem[0]      )].reshape(-1,2) 
        
        #leftMostPointsFromLeftBoxes=codeBoxArray[:,0,3].flatten() [np.flatnonzero(codeBoxArray[:,0,3])].reshape(-1,2)
        (vxLMB,vyLMB,xLMB,yLMB) = cv2.fitLine(topMostPoints,cv2.DIST_FAIR,0,0.01,0.01).flatten()
        yInterceptInformation=graphLine1(img,vxLMB,vyLMB,xLMB,yLMB,(0,255,0),3,topMostPoints)
        #delme=graphLine2(img,vxLMB,vyLMB,xLMB,yLMB,(0,255,0),3,topMostPoints)
        cntsYIntercepts.append( ((yInterceptInformation[0][1]),cntItem) )
        
    cv2.imwrite('c:\\temp\\associmage.png',img)
    sorted_cntsYIntercepts = sorted(cntsYIntercepts, key=lambda tup: tup[0]) 

    
    yDistanceBetweenContiguousCodes=999
   
    # presumes at least one rows where 2 succ codes are correctly identified
    for i in range(len(sorted_cntsYIntercepts)-1):
        y1=sorted_cntsYIntercepts[i][0]
        y2=sorted_cntsYIntercepts[i+1][0]
        gap= abs(y2-y1)
        if (gap>maxYInterceptGapForRow) & (gap<yDistanceBetweenContiguousCodes):
            yDistanceBetweenContiguousCodes=gap
    
    start=sorted_cntsYIntercepts[0][0]       
    numRows=int(abs(sorted_cntsYIntercepts[len(sorted_cntsYIntercepts)-1][0]-start+0.5*yDistanceBetweenContiguousCodes)/yDistanceBetweenContiguousCodes)+1
    codeBoxArray1=np.zeros( (numRows,2,4,500, 2) ,dtype = np.int32)
    
    
    start=sorted_cntsYIntercepts[0][0]
    for i in range(len(sorted_cntsYIntercepts)):
        rowNumber=abs(sorted_cntsYIntercepts[i][0]-start+0.5*yDistanceBetweenContiguousCodes)/yDistanceBetweenContiguousCodes
#        print "\n row #  value", int(rowNumber), " ", sorted_cntsYIntercepts[i][0], " ", (start+0.5*yDistanceBetweenContiguousCodes)
        if sorted_cntsYIntercepts[i][1][1,0,0] > 1300:
            codeBoxArray1[int(rowNumber),1]=sorted_cntsYIntercepts[i][1]
        else:
            codeBoxArray1[int(rowNumber),0]=sorted_cntsYIntercepts[i][1]
    
      
    

    return ( codeBoxArray1)
    
   
#    scntsRightSorted = sorted(cntsRight, key = lambda x: x[0,0,1],reverse=False)
#    scntsLeftSorted = sorted(cntsLeft, key = lambda x: x[0,0,1],reverse=False)
    

def decodeBox(img,lengthOfInnerSquare,offsetBorder):
    # convert image to grayscale and threshold to b/w
    gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    (thresh, im_bw) = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
   

    
    sideLengthInPixels =im_bw.shape[0]
    sideLengthInMM = (2 * offsetBorder) + (5* lengthOfInnerSquare)
    offsetBorderInPixels = sideLengthInPixels * ( offsetBorder/ sideLengthInMM)
    sideOfCodeInPixels= sideLengthInPixels-2*offsetBorderInPixels
    sideOfSubCodeInPixels = sideOfCodeInPixels/5
#    
    codeTally=0
    cnt=0
    for colNum in range(5):
        for rowNum in range(5):
            i1=int(offsetBorderInPixels+rowNum*sideOfSubCodeInPixels)
            i2=int(offsetBorderInPixels+(rowNum+1)*sideOfSubCodeInPixels)
            i3=int(offsetBorderInPixels+(colNum)*sideOfSubCodeInPixels)
            i4=int(offsetBorderInPixels+(colNum+1)*sideOfSubCodeInPixels)
            smallcode=im_bw[i1:i2,i3:i4]
            avg=np.average(smallcode)
            if (avg>128) or ( (rowNum==2) and (colNum==2)):
                cnt=cnt+1
                codeTally=codeTally+np.power(2,(rowNum+colNum*5))
            #codeTally=codeTally+
            

    return (codeTally)
 
       
    
    


        
def processPhoto(codeDir,idir,fx,fy,cx,cy,k1,k2,k3,tangd1,tangd2):    
#    filename="C:\\Projects\\tireProgramming\\get-perspective-transform-example\\images\\circleinrectpersp4.png"
#    filename="C:\\temp\\targets\\targetpic2.png"
#    filename="C:\\temp\\targets\\targetpic6.png"
#    filename="C:\\temp\\targets\\circprect3.png"
#    filename="C:\\temp\\targets\\circtarget2.png"
#    filename="C:\\temp\\targets\\smalltargetsR.png"
#    filename="C:\\temp\\targets\\circtarget3R.png"
#    filename="c:\\temp\\abh1.png"
#    filename="c:\\temp\\targets\\targets4.png"
#    filename="c:\\temp\\targets\\targets5.png"
#    filename="c:\\temp\\targets\\targets5bad1.png"
    
    
    
    #
    ## construct the argument parse and parse the arguments
    #ap = argparse.ArgumentParser()
    #ap.add_argument("-i", "--image", help = "path to the image file")
    #ap.add_argument("-c", "--coords",
    #	help = "comma seperated list of source points")
    #args = vars(ap.parse_args())
    #
    ## load the image and grab the source coordinates (i.e. the list of
    ## of (x, y) points)
    ## NOTE: using the 'eval' function is bad form, but for this example
    ## let's just roll with it -- in future posts I'll show you how to
    ## automatically determine the coordinates without pre-supplying them
    
    filename = codeDir+idir
    p1=filename.find("IMG")
    p2=filename.find(".")
    imageNumber=filename[p1+len("IMG")-1:p2]
    
    print ("\n Image number is ", imageNumber)
    
    print ("\n file is ", filename,"\n")
    
   
    image = cv2.imread(filename)
    
    
    
    
    
    
  
    
    
    K = np.array([[fx,0,cx], [0, fy, cy], [0, 0, 1]])
    #K = np.array([[fx,0,0], [0, fy, 0], [cx, cy, 1]])
    d = np.array([k1,k2, 0, 0,k3]) # just use first two terms (no translation)
    #d=0
    
    
    h, w = image.shape[:2]
    
    # undistort
    newcamera, roi = cv2.getOptimalNewCameraMatrix(K, d, (w,h), 0) 
    undistimg = cv2.undistort(image, K, d, None, newcamera)
    
    
    
    
    
    cv2.imwrite('c:\\temp\\image.png',image)
    cv2.imwrite('c:\\temp\\undistimage.png',undistimg)
    
    
    gray=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    
    gray = np.float32(gray)
    dst = cv2.cornerHarris(gray,2,3,0.04)
    
    
    
    dst = cv2.dilate(dst,None)
    ret, dst = cv2.threshold(dst,0.01*dst.max(),255,0)
    #ret, dst = cv2.threshold(dst,0.2*dst.max(),255,0)
    dst = np.uint8(dst)
    
    #imsdup = np.empty_like (undistimg)
    imsdup=np.zeros_like(undistimg)
    #imsdup[:] = undistimg
    
    imsdup1 = np.empty_like (undistimg)
    imsdup1[:] = undistimg
    
    
    
    gray1=cv2.cvtColor(undistimg,cv2.COLOR_BGR2GRAY)
    #ret,thresh = cv2.threshold(gray,127,255,1)
    ret,thresh = cv2.threshold(gray1,128,255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    cv2.imwrite('c:\\temp\\thresh.png',thresh)
    
    _,contours,h= cv2.findContours(thresh, cv2.RETR_TREE , cv2.CHAIN_APPROX_SIMPLE)
    scnts4=[cntitem1 for cntitem1 in contours if len(cv2.approxPolyDP(cntitem1,0.01*cv2.arcLength(cntitem1,True),True))==4] 
    
     
    
    cv2.drawContours(imsdup, scnts4, -1, (255, 0, 0), 3)
    cv2.imwrite("c:\\temp\\cont4sides.png",imsdup)
    
    #scnts4WithLength=[(cntitem1,   cv2.arcLength(cntitem1,True)  ) for cntitem1 in contours if len(cv2.approxPolyDP(cntitem1,0.01*cv2.arcLength(cntitem1,True),True))==4] 

    scnts4WithLength=[(cntitem1,   cv2.arcLength(cntitem1,True)  ) for cntitem1 in contours if ( (len(cv2.approxPolyDP(cntitem1,0.01*cv2.arcLength(cntitem1,True),True))==4 ) and (cv2.contourArea(cntitem1)>1300) )] 
    contlengths= np.asarray([cv2.arcLength(cntitem1,True) for cntid,cntitem1 in enumerate(scnts4) ])
    binsForContours=np.arange(500,3200,600)
    hist1=np.histogram(contlengths, bins=binsForContours)
    maxIndex=hist1[0].argmax()
    approxCodePerimeterLow=600*maxIndex+500
    approxCodePerimeterHigh=600*(maxIndex+1)+500
    codeBoxPerimThresh = 0.25 * 600
    contoursCodesOnly= [cntitem[0] for cntitem in scnts4WithLength if (cntitem[1]> approxCodePerimeterLow-codeBoxPerimThresh) & (cntitem[1]<approxCodePerimeterHigh+codeBoxPerimThresh) ]
    
    
    
    cv2.drawContours(imsdup, contoursCodesOnly, -1, (255, 0, 0), 3)
    cv2.imwrite("c:\\temp\\contimg11.png",imsdup)
    
    
    
    #numberOfRows=10
#    numberOfRows=9
    #maxNumberOfPtsToAggregate=5000
    maxNumberOfPtsPerSide=500
    
    numberOfBoxes=len(contoursCodesOnly)
    
    
    codeBoxArray1L=np.zeros(  (numberOfBoxes,4,maxNumberOfPtsPerSide, 2) ,dtype = np.int32)
    #codeBoxCornerInterval1L=np.zeros( (len(scnt6a) ,4,2,2) ,dtype = np.int32)
    codeBoxCorners1L=np.zeros( (numberOfBoxes ,4,2) ,dtype = np.float32)
    
    #ptsAggregated=np.zeros( (maxNumberOfPtsToAggregate,2), dtype = np.int32)
    
    for boxID,boxItem  in enumerate(contoursCodesOnly):
        boxFormContourBox1(boxItem,boxID, codeBoxArray1L,maxNumberOfPtsPerSide)     
#    
#    codeBoxArray = AssociateBoxesWithRowsColumns(undistimg,codeBoxArray1L)
#    (numberOfRows,numberOfColumns)=(codeBoxArray.shape[0],codeBoxArray.shape[1])
#    codeBoxCorners1L=np.zeros((numberOfRows,numberOfColumns ,4,2) ,dtype = np.float32)
#    codeBoxLocalCenter=np.zeros( (numberOfRows,2,2) ,dtype = np.float32)
#    codeBoxExactCenter=np.zeros( (numberOfRows,2,2) ,dtype = np.float32)
#  
    
   
    #codeBoxCorners1L=np.zeros((numberOfRows,numberOfColumns ,4,2) ,dtype = np.float32)
    #numberOfRows=100
    codeBoxLocalCenter=np.zeros( (numberOfBoxes,2) ,dtype = np.float32)
    codeBoxExactCenter=np.zeros( (numberOfBoxes,2) ,dtype = np.float32)
    codeBoxCode=np.zeros( (numberOfBoxes) ,dtype = np.int32)
  

    
    
    
    totalErrorTally=0
    codeList=[]
    
    for boxNumber in range(numberOfBoxes):
            if (np.count_nonzero(codeBoxArray1L[boxNumber])==0):
#                print "\n 77777777777 Continue"
                continue
            print ("\n ******** Box Number ", boxNumber)
            leftMostPointsFromBox=codeBoxArray1L[boxNumber,3].flatten() [np.flatnonzero(codeBoxArray1L[boxNumber,3])].reshape(-1,2)
            rightMostPointsFromBox=codeBoxArray1L[boxNumber,1].flatten() [np.flatnonzero(codeBoxArray1L[boxNumber,1])].reshape(-1,2)
            topMostPointsFromBox=codeBoxArray1L[boxNumber,0].flatten() [np.flatnonzero(codeBoxArray1L[boxNumber,0])].reshape(-1,2)
            bottomMostPointsFromBox=codeBoxArray1L[boxNumber,2].flatten() [np.flatnonzero(codeBoxArray1L[boxNumber,2])].reshape(-1,2)
          
    
          
            [vxLM,vyLM,xLM,yLM] = cv2.fitLine(leftMostPointsFromBox,cv2.DIST_FAIR,0,0.01,0.01).flatten()
            #print "\n $$$$$$$$$$$$$ rightMostPointsFromBox\n",rightMostPointsFromBox
            [vxRM,vyRM,xRM,yRM] = cv2.fitLine(rightMostPointsFromBox,cv2.DIST_FAIR,0,0.01,0.01).flatten()
            [vxTM,vyTM,xTM,yTM] = cv2.fitLine(topMostPointsFromBox,cv2.DIST_FAIR,0,0.01,0.01).flatten()
            [vxBM,vyBM,xBM,yBM] = cv2.fitLine(bottomMostPointsFromBox,cv2.DIST_FAIR,0,0.01,0.01).flatten()
    
        
            topMost=graphLine1(undistimg,vxTM,vyTM,xTM,yTM,(255,255,0),0.5,leftMostPointsFromBox)
            #print "\n *****TM****", rowNumber, columnNumber,vxTM,vyTM,xTM,yTM
            rightMost=graphLine1(undistimg,vxRM,vyRM,xRM,yRM,(255,0,0),0.5,rightMostPointsFromBox)
            #print "\n *****RM****", rowNumber, columnNumber, vxRM,vyRM,xRM,yRM
            bottomMost=graphLine1(undistimg,vxBM,vyBM,xBM,yBM,(0,255,255),0.5,topMostPointsFromBox)
            #print "\n *****BM****", rowNumber, columnNumber, vxBM,vyBM,xBM,yBM
            leftMost=graphLine1(undistimg,vxLM,vyLM,xLM,yLM,(0,0,255),0.5,bottomMostPointsFromBox)
            #print "\n *****LM******", rowNumber, columnNumber,vxLM,vyLM,xLM,yLM
            
            topLeft=line_intersection( leftMost, topMost)
            topRight=line_intersection( rightMost, topMost)
            bottomRight=line_intersection( rightMost, bottomMost)
            bottomLeft=line_intersection( leftMost, bottomMost)
            
#            print "\n Corners of Box", topLeft, topRight, bottomRight, bottomLeft
           
            
            codeBoxCorners1L[boxNumber,0]= np.array([ int(topLeft[0]), int(topLeft[1])          ])
            codeBoxCorners1L[boxNumber,1]= np.array([ int(topRight[0]), int(topRight[1])          ])
            codeBoxCorners1L[boxNumber,2]= np.array([ int(bottomRight[0]), int(bottomRight[1])          ])
            codeBoxCorners1L[boxNumber,3]= np.array([ int(bottomLeft[0]), int(bottomLeft[1])          ])
            
            print ("\n**** ", codeDir, idir, "\n **** Corners",codeBoxCorners1L[boxNumber,0],codeBoxCorners1L[boxNumber,1],codeBoxCorners1L[boxNumber,2],codeBoxCorners1L[boxNumber,3],"\n" )
           
          
          
         
            
#            lengthOfInnerSquare=1.7
#            offsetBorder=2.2
    
            
#            print "\n &&&&&&&&&&&& ", xCenterPos, " ", yCenterPos, " ", rowNumber, " ", columnNumber
            
            
            
            centerSmall = line_intersection(  (  topLeft, bottomRight     ), ( topRight,  bottomLeft     ) )
            codeBoxLocalCenter[boxNumber]=np.array([centerSmall[0],  centerSmall[1]]) 
            #codeBoxExactCenter[rowNumber,columnNumber]=np.array([ xCenterPos,yCenterPos     ])
            
    
             
            destBox1=np.array([  
                [0,0  ],
                [0,1000 ],
                [1000,1000 ],
                [1000,0  ],
              
                ],dtype = "float32")
    
    
                             
            localBoxTransform= cv2.getPerspectiveTransform(  codeBoxCorners1L[boxNumber] ,  destBox1    )
            #localBoxImg=np.zeros( (500,500,3) ,dtype = np.float32)
            filenamebox="c:\\temp\\localBox"+str(boxNumber)+"_.jpg"
            warp = cv2.warpPerspective(imsdup1,localBoxTransform,(1000,1000))
            cv2.imwrite(filenamebox, warp)
            
            code=decodeBox(warp,lengthOfInnerSquare,offsetBorder)
            codeBoxCode[boxNumber]=code
            if str(code) not in markerDictionary:
                codeBoxLocalCenter[boxNumber]=np.array([   0,0       ]) 
                continue
            
            centerSmall = line_intersection(  (  topLeft, bottomRight     ), ( topRight,  bottomLeft     ) )
            codeBoxLocalCenter[boxNumber]=np.array([centerSmall[0],  centerSmall[1]]) 
            
            codeBoxExactCenter[boxNumber]=np.array((float(markerDictionary[str(code)][0]),float(markerDictionary[str(code)][1])       )).reshape(-1,2)

            codeList.append(code)
            
            
    xfudgeFactor=200
  
    middleX=np.average(codeBoxCorners1L[:,0] [:,0])
    leftBoxes=np.where( codeBoxCorners1L[:,0] [:,0] <middleX)
    rightBoxes=np.where( codeBoxCorners1L[:,0] [:,0] >middleX)
    maxXLeft=max(codeBoxCorners1L[:,0] [:,0][leftBoxes])
    #minXLeft=min(codeBoxCorners1L[:,0] [:,0][leftBoxes])
    minyLeft=min(codeBoxCorners1L[:,0] [:,1][leftBoxes])
    maxyLeft=max(codeBoxCorners1L[:,0] [:,1][leftBoxes])
    
    #maxXRight=max(codeBoxCorners1L[:,0] [:,0][rightBoxes])
    minXRight=min(codeBoxCorners1L[:,0] [:,0][rightBoxes])
    minyRight=min(codeBoxCorners1L[:,0] [:,1][rightBoxes])
    maxyRight=max(codeBoxCorners1L[:,0] [:,1][rightBoxes])
    
    pt1 = (int(maxXLeft-xfudgeFactor), max(minyLeft,minyRight) )
    pt2= (int(minXRight+xfudgeFactor), min( maxyLeft,maxyRight ))
    
    
    
       # need to redistort corners for the mask
    mask = np.zeros_like (image)
    # fill rectangle with whitle
   
    cv2.rectangle(mask, pt1,pt2 ,   (255,255,255) ,-1         )
    cv2.imwrite(codeDir+"M"+idir, mask)
    cv2.imwrite("c:\\temp\\M"+idir, mask)
    
            
       
           
    
    

    cbec=codeBoxExactCenter.flatten() [np.flatnonzero(codeBoxExactCenter)].reshape(-1,2)
    cblc=codeBoxLocalCenter.flatten() [np.flatnonzero(codeBoxLocalCenter)].reshape(-1,2)
    numNonZeroCodes=cblc.shape[0]
    
    #perspTrAllPts,mask=cv2.findHomography(codeBoxLocalCenter.reshape(-1,2), codeBoxExactCenter.reshape(-1,2) )
    perspTrAllPts,mask=cv2.findHomography(cblc,cbec)
    #cbmc=np.zeros_like( cbec ,dtype = np.float32)
    
    #codeBoxMappedCenter=cv2.perspectiveTransform( cblc.reshape(1,numberOfRows*2,2),perspTrAllPts)
    cbmc=cv2.perspectiveTransform( cblc.reshape(1,numNonZeroCodes,2),perspTrAllPts)
    
    

    
    imgUndist = np.empty_like (undistimg)
    imgUndist[:] = undistimg
    filenameperscorrected="c:\\temp\\IMGPersCorr.jpg"
    warp = cv2.warpPerspective(imgUndist,perspTrAllPts,(3264,2448))
    cv2.imwrite(filenameperscorrected, warp)
    
    
    
    distResult=spsd.cdist(cbmc.reshape(-1,2),cbec.reshape(-1,2) )
    
    rmsError=np.sqrt(totalErrorTally)/(numberOfBoxes*2)
    
    perspTrAllPtsInv=np.linalg.inv(perspTrAllPts)
    cbinferredcenters=cv2.perspectiveTransform( cbec.reshape(1,numNonZeroCodes,2),perspTrAllPtsInv)
   
    print ("\nRMS ",rmsError)
    
    # generate visual of error by drawing circles around the error (based on filename and undistimg)
    showErrorsFileName=filename
    showErrorsFileName=showErrorsFileName.replace("IMG_", "IMAGEcenter_")
    
    showErrors = np.empty_like (undistimg)
    showErrors[:] = undistimg
    
    for numcodes in range(len(codeList)):
    # for each target - draw a circle around the center
        clr=(255,0,255)
        resultP1=( (     int(cbinferredcenters[0,numcodes,0])        , int(cbinferredcenters[0,numcodes,1]) ) )
        print ("\n Pt is ", resultP1)
        
        cv2.circle(showErrors,resultP1,8 ,clr,1)
        #cv2.circle(showErrors,resultP1,8 ,clr,2)
        errorForCode=distResult[numcodes,numcodes]
       
        resultP1TextError= ( (     int(cbinferredcenters[0,numcodes,0]+110)        , int(cbinferredcenters[0,numcodes,1]) ) )
        resultP1TextCode = ( (     int(cbinferredcenters[0,numcodes,0]-310)        , int(cbinferredcenters[0,numcodes,1]) ) )


        #cv2.putText(showErrors,str(errorForCode),resultP1Text,cv2.FONT_HERSHEY_SIMPLEX,clr)
        cv2.putText(showErrors,str(errorForCode), resultP1TextError, cv2.FONT_HERSHEY_SIMPLEX, 1, 255)
        cv2.putText(showErrors,str(codeList[numcodes]), resultP1TextCode, cv2.FONT_HERSHEY_SIMPLEX, 1, 255)
        
    print ("\n*** SHow error file name is", showErrorsFileName)
    cv2.imwrite(showErrorsFileName, showErrors)
    
    
    
#    print "\n\n777777777777\n" , topLeft,topRight,bottomLeft,bottomRight
    
    
    
    cv2.imwrite("c:\\temp\\lineq.png", undistimg)
    
    return ( (codeList, cbinferredcenters, distResult) )


def getExifInformation(codeDir):

    for idir in os.listdir(codeDir):
        print ("\n",idir)
        #if idir.endswith("JPG") and idir.startswith("IMG_"):
        if re.search("IMG.+\.[jP][pP][gG]",idir):
            img = PIL.Image.open(codeDir+idir)
         
            exif = {
                PIL.ExifTags.TAGS[k]: v
                for k, v in img._getexif().items()
                if k in PIL.ExifTags.TAGS
             }
            print ("\n*", idir)
            return(exif)   

################# main starts here ##########

#codeDir is directory that contains photos of the form IMG_DDDDD
#also contains metadata files that contains geometry of markers (markersMetaData.csv)


codeDir=sys.argv[1]
calibrationInfo = sys.argv[2]

exifData = getExifInformation(codeDir)
exifTAData={}
exifTAData['Model']=exifData['Model']
exifTAData['ExifImageWidth']=exifData['ExifImageWidth'][0]
exifTAData['ExifImageHeight']=exifData['ExifImageHeight'][0]

distortionParams={}

distortionParams['iPhone 4S','checkboard7x11','fx'] = 3.08768833e+03 
distortionParams['iPhone 4S','checkboard7x11','fy'] = 3.08786068e+03  
distortionParams['iPhone 4S','checkboard7x11','cx'] = 1.61822175e+03
distortionParams['iPhone 4S','checkboard7x11','cy'] = 1.26702669e+03
distortionParams['iPhone 4S','checkboard7x11','k1'] = 0.23148765 
distortionParams['iPhone 4S','checkboard7x11','k2'] = 0.51836559   
distortionParams['iPhone 4S','checkboard7x11','k3'] = -0.48297284 

distortionParams['SAMSUNG-SM-G890A','checkboard7x11','fx'] = 3.99411182e+03 
distortionParams['SAMSUNG-SM-G890A','checkboard7x11','fy'] = 3.99418122e+03 
distortionParams['SAMSUNG-SM-G890A','checkboard7x11','cx'] = 2.68713926e+03
distortionParams['SAMSUNG-SM-G890A','checkboard7x11','cy'] = 1.51055154e+03
distortionParams['SAMSUNG-SM-G890A','checkboard7x11','k1'] = 0.24503953  
distortionParams['SAMSUNG-SM-G890A','checkboard7x11','k2'] = -0.80636859 
distortionParams['SAMSUNG-SM-G890A','checkboard7x11','k3'] = 0.77637451

distortionParams['XT1063','checkboard7x11','fx'] = 2611 
distortionParams['XT1063','checkboard7x11','fy'] = 2624 
distortionParams['XT1063','checkboard7x11','cx'] =1633
distortionParams['XT1063','checkboard7x11','cy'] = 1232
distortionParams['XT1063','checkboard7x11','k1'] = 0.13
distortionParams['XT1063','checkboard7x11','k2'] = -0.41
distortionParams['XT1063','checkboard7x11','k3'] = 0.332

# set the appropriate distortion parameters

fx=distortionParams[ exifTAData['Model'], calibrationInfo,'fx']
fy=distortionParams[ exifTAData['Model'], calibrationInfo,'fy']
cx=distortionParams[ exifTAData['Model'], calibrationInfo,'cx']
cy=distortionParams[ exifTAData['Model'], calibrationInfo,'cy']
k1=distortionParams[ exifTAData['Model'], calibrationInfo,'k1']
k2=distortionParams[ exifTAData['Model'], calibrationInfo,'k2']
k3=distortionParams[ exifTAData['Model'], calibrationInfo,'k3']

tangd1=0
tangd2=0


markernamePosList=[]
markerInformation=codeDir+"markersMetadata.csv"

markerDictionary={}
with open(markerInformation, 'rt') as f:  
    reader = csv.reader(f, delimiter=',' , quoting=csv.QUOTE_NONE)
    
    #reader = csv.reader(f)
    for row in reader:
        if (len(row)>3):
            print( "hello")
            row1=row
            (a,b,c,d,e,f,g,h,i,j,k)=row
            lengthOfInnerSquare=float(a)
            offsetBorder=float(b)
            sizeOfCheckerboardPattern=int(c)
            numberOfCheckerboardRows=int(d)
            numberOfCodes=int(e)
            verticalDistanceBetweenCodeCenters=float(f)
            horizontalDistanceBetweenCodeCenters=float(g)
            heightOfCodesColumn=float(h)
            widthOfCodesRow=float(i)
            pageHeight=float(j)
            pageWidth=float(k) 
        else:
            (codeName, xcenter_mm,ycenter_mm)=row
            markerDictionary[codeName]=(xcenter_mm,ycenter_mm)
            
        print ("\nRow is", row," ", len(row),"\n")
   

def decodeValue(s):
    try:
        float(s)
        return(float(s))
    except ValueError:
        try: 
            int(s)
            return(int(s))
        except ValueError:
            return(s)
            

markerErrorInformation={}
codeToIndexMapper={}
indexToCodeMapper={}

for i,(key,value) in enumerate(markerDictionary.iteritems()):
    codeToIndexMapper[key]=i
    indexToCodeMapper[i]=key
    
fileIndex=0

photoToIndexMapper={}
indexToPhotoMapper={}
for idir in os.listdir(codeDir):
    print (idir)
    if idir.endswith("JPG") and idir.startswith("IMG_"):
        print ("\n got here")
        filenameWithPath=codeDir+idir
        photoToIndexMapper[filenameWithPath]=fileIndex
        indexToPhotoMapper[fileIndex]=filenameWithPath
        fileIndex=fileIndex+1
        


codeStats=np.zeros( (len(markerDictionary), fileIndex), dtype=(float,3))



codeData= codeDir+ "markerLocations.txt"
totalGoodPhotos=0
totalBadPhotos=0
f = open(codeData,'w')
for idir in os.listdir(codeDir):
    
    if idir.endswith("jpg") and idir.startswith("IMG_"): 
        filenameWithPath = codeDir+idir
        #(codeList, cbmc, distResult) = processPhoto("c:\\temp\\targets\\live\\targets5bad1.png")
       
        (codeList, cbmc, distResult) = processPhoto( codeDir,idir ,fx,fy,cx,cy,k1,k2,k3,tangd1,tangd2)
        avgError= np.mean(np.diagonal(distResult))
        if (avgError>1):
            totalBadPhotos=totalBadPhotos+1
            continue
        totalGoodPhotos=totalGoodPhotos+1
        print( "\n ******************** Avg Error ", avgError)
           

      

        for numcodes in range(len(codeList)):
            distPx,distPy=Distort(cbmc[0,numcodes,0],cbmc[0,numcodes,1],fx,fy,cx,cy,k1,k2,k3,tangd1,tangd2)
            

            
            print (filenameWithPath, ",", codeList[numcodes],",",int(cbmc[0,numcodes,0]), ",",int(cbmc[0,numcodes,1]), ",",distResult[numcodes,numcodes]),",",30,",",40
            #print (filenameWithPath, ",", codeList[numcodes],",",int(cbmc[0,numcodes,0]), ",",int(cbmc[0,numcodes,1]), ",",distResult[numcodes,numcodes],file=f)


            printList = [filenameWithPath,codeList[numcodes], cbmc[0,numcodes,0], cbmc[0,numcodes,1], distResult[numcodes,numcodes] ,distPx,distPy]
            printList = ','.join(map(str, printList)) 
            print (printList,file=f)
            


f.close()


    


